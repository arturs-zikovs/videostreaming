//
//  AppDelegate.h
//  videostreaming
//
//  Created by Artur on 09/04/2018.
//  Copyright © 2018 company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

