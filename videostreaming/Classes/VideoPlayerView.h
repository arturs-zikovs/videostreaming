//
//  VideoPlayerView.h
//  videostreaming
//
//  Created by Artur on 09/04/2018.
//  Copyright © 2018 company. All rights reserved.
//

#import <UIKit/UIKit.h>

@import AVKit;
@import AVFoundation;

@interface VideoPlayerView : UIView

//Variables
@property AVPlayer                  *player;
@property (readonly) AVPlayerLayer  *playerLayer;

@end
