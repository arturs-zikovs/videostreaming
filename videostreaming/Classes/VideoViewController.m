//
//  VideoViewController.m
//  videostreaming
//
//  Created by Artur on 09/04/2018.
//  Copyright © 2018 company. All rights reserved.
//

#import "VideoViewController.h"
#import "VideoViewControllerDefines.h"

#import "VideoPlayerView.h"

@import AVKit;
@import AVFoundation;
@import MediaPlayer;

@interface VideoViewController ()

//Variables
@property (nonatomic,strong) AVPlayerItem       *playerItem;
@property (nonatomic, strong) NSMutableArray    *playlistArray;
@property (nonatomic, strong) NSTimer           *overlayTimer;

//IBOutlets
@property (weak, nonatomic) IBOutlet UILabel             *videoResolutionLabel;
@property (weak, nonatomic) IBOutlet UILabel             *videoDurationLabel;
@property (weak, nonatomic) IBOutlet VideoPlayerView     *videoContainerView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView    *videoLoadIndicator;
@property (weak, nonatomic) IBOutlet UIView                     *overlayView;
@property (weak, nonatomic) IBOutlet UIButton                   *playButton;

@property (nonatomic) NSInteger     numberOfVideoPlayed;

@end

@implementation VideoViewController

#pragma mark - Stream data management

- (void)loadStreamsFromServer {
    
    self.numberOfVideoPlayed = 0;
    
    NSString *URLString = [NSString stringWithFormat:kLiveStreamingURL];
    
    //Creating block to finalize data load
    void (^dataDownloadedBlock)(void) = ^(){
        
        //Initializing stream playback
        [self loadPlayerViewWithVideoURL: self.playlistArray[self.numberOfVideoPlayed]];
        
        //Initializing player controls overlay
        [self initializeAirplayView];
        
    };
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithURL:[NSURL URLWithString:URLString]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if (error != nil) {
                    // error appeared
                    NSLog(@"error in NSURLSession is %@", [error localizedDescription]);
                    
                    //Loading local data
                    self.playlistArray = [[[NSArray alloc] initWithObjects:kLiveStreamingURL1Test, kLiveStreamingURL2Test, nil] mutableCopy];
                }
                else {
                    if (data) {
                        NSError *error = nil;
                        
                        //Initializing playlist and parsing data
                        self.playlistArray = [[NSArray array] mutableCopy];
                        NSMutableArray *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        if ([jsonData count]) {
                            for (NSDictionary *jsonObject in jsonData) {
                                [self.playlistArray addObject:jsonObject[@"url"]];
                            }
                        }
                        else {
                            //Loading local data
                            self.playlistArray = [[[NSArray alloc] initWithObjects:kLiveStreamingURL1Test, kLiveStreamingURL2Test, nil] mutableCopy];

                        }
                    }
                }
                
                BLOCK_SAFE_PERFORM(dataDownloadedBlock);
                
            }] resume];
}

#pragma mark - Video player management

- (void)loadPlayerViewWithVideoURL:(NSString *)videoURLString {
    //Initializing player item
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:videoURLString]];
    
    self.videoContainerView.player = [AVPlayer playerWithPlayerItem:playerItem];
    self.videoContainerView.player.allowsExternalPlayback = YES;
    
    // Initialize playback when stream is loaded
    [self.videoContainerView.player.currentItem addObserver:self
                                                 forKeyPath:@"status"
                                                    options:0
                                                    context:nil];
    
    
    [self.videoContainerView.player.currentItem addObserver:self forKeyPath:@"timedMetadata" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.videoContainerView.player addObserver:self
                                     forKeyPath:@"rate"
                                        options:0
                                        context:nil];
    
    //Updating overlay and play button initial state
    self.playButton.tag = kPlayButtonTagHidden;
    [self overlayAreaTapped];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self.videoContainerView.player.currentItem && [keyPath isEqualToString:@"status"])
    {
        if (self.videoContainerView.player.currentItem.status == AVPlayerStatusReadyToPlay) {
            
            //Update visuals
            self.videoLoadIndicator.hidden = YES;
            self.playButton.hidden = NO;
            
            [self.videoContainerView.player play];
            
            //set to pause
            self.playButton.selected = YES;
            
            [self updateStreamMetadata];
        }
    }
    if ([keyPath isEqualToString:@"rate"]) {
        if (self.videoContainerView.player.timeControlStatus == AVPlayerTimeControlStatusPaused) {
            //set to pause
            self.playButton.selected = YES;
        }
        else if (self.videoContainerView.player.timeControlStatus==AVPlayerTimeControlStatusPlaying) {
            //set to play
            self.playButton.selected = NO;
        }
    }
}

- (void)initializeAirplayView {
    
    //if iOS 11+ then
    if (@available(iOS 11.0, *)) {
    
        //Initializing view for airplay
        AVRoutePickerView *routePickerView = [[AVRoutePickerView alloc] initWithFrame:CGRectMake(0,
                                                                                                 0,
                                                                                                 kAirplayButtonSize,
                                                                                                 kAirplayButtonSize)];
        
        [self.videoContainerView addSubview:routePickerView];
        routePickerView.backgroundColor = [UIColor blackColor];
    }
    else {
        //iOS 10.x and lower - need to implement MPVolumeView for earlier versions
    }
}

- (void)updateStreamMetadata {
    
    //Updating resolution
    self.videoResolutionLabel.text = [NSString stringWithFormat:@"%.f px x %.f px", self.videoContainerView.playerLayer.videoRect.size.width,
                                                                            self.videoContainerView.playerLayer.videoRect.size.height];
    
    //Updating duration
    CMTime duration = self.videoContainerView.player.currentItem.asset.duration;
    float seconds = CMTimeGetSeconds(duration);
    self.videoDurationLabel.text = [NSString stringWithFormat:@"%.2f seconds", seconds];

    
    NSArray *logEvents=self.playerItem.accessLog.events;
    AVPlayerItemAccessLogEvent *event = (AVPlayerItemAccessLogEvent *)[logEvents lastObject];
    double bitRate=event.observedBitrate;
    
    NSLog(@"%f", bitRate);
}

#pragma mark - Player controls

- (IBAction)playButtonTapped:(id)sender {
    
    //If player is playing
    if (self.videoContainerView.player.timeControlStatus == AVPlayerTimeControlStatusPaused) {
        
        //if playback ended, move to beginning
        if (CMTimeGetSeconds(self.videoContainerView.player.currentTime) >=
            CMTimeGetSeconds(self.videoContainerView.player.currentItem.duration)) {
            [self.videoContainerView.player seekToTime:kCMTimeZero];
        }
        
        [self.videoContainerView.player play];
        
        //Updating to pause
        self.playButton.selected = YES;
    }
    else if (self.videoContainerView.player.timeControlStatus==AVPlayerTimeControlStatusPlaying) {
        
        [self.videoContainerView.player pause];
        
        //Updating to play
        self.playButton.selected = NO;
    }
}

- (IBAction)overlayAreaTapped {
    
    //clean up timer if it was active
    [self.overlayTimer invalidate];
    
    if (self.playButton.tag == kPlayButtonTagHidden) {
        
        //Show button if it was hidden
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.playButton.alpha = 1;
                         }
                         completion:^(BOOL finished) {
                            
                             self.playButton.tag = kPlayButtonTagShown;
                             
                             //Set timer for 3 seconds to hide controls
                             self.overlayTimer = [NSTimer scheduledTimerWithTimeInterval:3.0
                                                                                  target:self
                                                                                selector:@selector(overlayAreaTapped)
                                                                                userInfo:nil
                                                                                 repeats:NO];
                         }];
    }
    else {
        
        //Hide button if it was shown before
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.playButton.alpha = 0;
                         }
                         completion:^(BOOL finished) {
                             self.playButton.tag = kPlayButtonTagHidden;
                         }];
    }
}

- (IBAction)overlayAreaSwiped:(id)sender {
    
    //Preparing visuals
    self.videoLoadIndicator.hidden = NO;
    self.playButton.hidden = YES;
    
    //Swipe to next video
    self.numberOfVideoPlayed++;
    
    if (self.numberOfVideoPlayed >= [self.playlistArray count]) {
        self.numberOfVideoPlayed = 0;
    }
    
    [self loadPlayerViewWithVideoURL:self.playlistArray[self.numberOfVideoPlayed]];
}

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Loading initial stream data
    [self loadStreamsFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
