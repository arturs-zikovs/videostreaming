//
//  VideoPlayerView.m
//  videostreaming
//
//  Created by Artur on 09/04/2018.
//  Copyright © 2018 company. All rights reserved.
//

#import "VideoPlayerView.h"

// PlayerView.m
@implementation VideoPlayerView


- (AVPlayer *)player {
    return self.playerLayer.player;
}

- (void)setPlayer:(AVPlayer *)player {
    self.playerLayer.player = player;
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
}

// Override UIView method
+ (Class)layerClass {
    return [AVPlayerLayer class];
}

- (AVPlayerLayer *)playerLayer {
    return (AVPlayerLayer *)self.layer;
}
@end
