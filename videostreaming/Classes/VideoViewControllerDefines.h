//
//  VideoViewControllerDefines.h
//  videostreaming
//
//  Created by Artur on 09/04/2018.
//  Copyright © 2018 company. All rights reserved.
//

#ifndef VideoViewControllerDefines_h
#define VideoViewControllerDefines_h

//test purposes only
#define kLiveStreamingURL1Test  @"https://bitmovin-a.akamaihd.net/content/playhouse-vr/m3u8s/105560.m3u8"
#define kLiveStreamingURL2Test  @"https://devimages-cdn.apple.com/samplecode/avfoundationMedia/AVFoundationQueuePlayer_HLS2/master.m3u8"

#define kLiveStreamingURL       @"http://www.json-generator.com/api/json/get/ceIVmaebci?indent=2"

#define kAirplayButtonSize  30.0f

#define kPlayButtonTagHidden    0
#define kPlayButtonTagShown     1

//technical stuff
#define BLOCK_SAFE_PERFORM(block, ...) block ? block(__VA_ARGS__) : nil

#endif /* VideoViewControllerDefines_h */
